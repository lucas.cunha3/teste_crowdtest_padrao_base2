using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;
using System.Threading;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CasosDeTesteTests : TestBase
    {

        #region Pages and Flows Objects

        LoginTests LoginTests;
        MainPage MainPage;

        DashboardPage DashboardPage;
        ProjectsPage ProjectsPage;

        CTProjectsPage CTProjectsPage;

        ProjectSelectedPage ProjectSelectedPage;

        #endregion

        [Test]
        public void SelecionarCasoDeTeste()
        {
          
          LoginTests = new LoginTests();
          MainPage = new MainPage();
          DashboardPage = new DashboardPage();
          ProjectsPage = new ProjectsPage();
          CTProjectsPage = new CTProjectsPage();
          ProjectSelectedPage = new ProjectSelectedPage();

          LoginTests.RealizarLoginComSucesso();
          MainPage.ClicarEmGerenciar();
          DashboardPage.ClicarEmProjetos();
          ProjectsPage.ClicarNoProjeto1();
          ProjectSelectedPage.ClicarNoBotaoCasoDeTesteDentroDeUmProjeto();
          CTProjectsPage.RetornaNumeroDoCasoDeTeste1();

          Assert.AreEqual("1",CTProjectsPage.RetornaNumeroDoCasoDeTeste1());

        }

    }
}
