﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class LoginTests : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        MainPage mainPage;
        #endregion

        [Test]
        public void RealizarLoginComSucesso()
        {
            loginPage = new LoginPage();
            mainPage = new MainPage();

            #region Parameters
            string usuario = "lucas.cunha@base2.com.br";
            string senha = "Cunha@2022";
            string mensagemDeLoginOk = "Bem-vindo ao Crowdtest! O que deseja fazer hoje?";
            #endregion

            loginPage.ClicarEmProsseguir();
            loginPage.PreencherUsuario(usuario);
            loginPage.PreencherSenha(senha);
            loginPage.ClicarEmLogin();
            
            Assert.AreEqual(mainPage.RetornaMensagemDeBemVindo(),mensagemDeLoginOk);
        }

    }
}
