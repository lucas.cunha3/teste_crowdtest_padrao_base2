using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class CTProjectsPage : PageBase
    {
        #region Mapping

        
        By casetest1InfoTextArea = By.XPath("//*[@class='mat-row ng-star-inserted'][1]//*[@class='mat-cell cdk-column-id mat-column-id ng-star-inserted']");


        #endregion

        #region Actions
        
        
        public string RetornaNumeroDoCasoDeTeste1()
        {
            return GetText(casetest1InfoTextArea);
        }
        #endregion
       
     
        
    }
}
