using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class DashboardPage : PageBase
    {
        #region Mapping

        
        By projectsButton = By.XPath("//*[@class='li-projects']");


        #endregion

        #region Actions
        
        
        public void ClicarEmProjetos()
        {
            Click(projectsButton);
        }
       
        #endregion
        
    }
}
