using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ProjectSelectedPage : PageBase
    {
        #region Mapping

        
        By caseTestButton = By.Id("mat-tab-label-0-2");


        #endregion

        #region Actions
        
        
        public void ClicarNoBotaoCasoDeTesteDentroDeUmProjeto()
        {
            Click(caseTestButton);
        }
       
        #endregion
        
    }
}
