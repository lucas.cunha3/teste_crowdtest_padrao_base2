﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping
        By welcomeInfoTextArea = By.XPath("//*[@class='row title']//span");
        
        By gerenciarButton = By.XPath("//*[@class='col-6 manager']//*[@class='btn btn-crowdtest mr-1']");
        By usernameLoginInfoTextArea = By.XPath("//td[@class='login-info-left']/span[@class='italic']");
        By reportIssueLink = By.XPath("//a[@href='/bug_report_page.php']");
        #endregion

        #region Actions
        
        
        public string RetornaMensagemDeBemVindo()
        {
            return GetText(welcomeInfoTextArea);
        }

        public void ClicarEmGerenciar()
        {
            Click(gerenciarButton);
        }
        public string RetornaUsernameDasInformacoesDeLogin()
        {
            return GetText(usernameLoginInfoTextArea);
        }

        public void ClicarEmReportIssue()
        {
            Click(reportIssueLink);
        }


        #endregion
        
    }
}
