using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ProjectsPage : PageBase
    {
        #region Mapping

        
        By project1Button = By.XPath("//*[@class='mat-cell cdk-column-name mat-column-name ng-star-inserted']");


        #endregion

        #region Actions
        
        
        public void ClicarNoProjeto1()
        {
            Click(project1Button);
        }
       
        #endregion
        
    }
}
